# Implementacion KOHA para diferentes entornos

**Título**
Implementacion KOHA para diferentes entornos

**Autor**
FRANCO GENARO - Universidad Nacional del Litoral - Tecnicatura universitaria en Software Libre

**Trayecto**
ADMINISTRACION

**Director**
MARTIN BAYO



**Resumen**
KOHA es un programa que le permite a una persona que administre una biblioteca, la gestión integral de la misma. Como así también la utilización por parte de los lectores



**Justificación**
Elegí la implementación de Koha para mi trabajo final de la carrera, aun sin conocerlo.
Un colega que trabaja en la administración publica me comento que la tenían ganas de implementarlo desde hace un tiempo y los intentos que tuvieron, fueron fallidos en múltiples oportunidades. Actualmente, incluso, lograron instalarlo, pero no se estaría utilizando.


 
**Estado del arte**


**¿Cuál es la función principal de Koha?**
Es un programa que le permite a una persona que administre una biblioteca, la gestión integral de la misma. Como así también la utilización por parte de los clientes.



**¿Existen otros programas que hacen lo mismo?**

Si, realmente son muchos e incluso, varios son de software libre. Entre los que podemos identificar, por ej.:

- ABCD (open Source)
 
- Aleph 500 (privativo)
 
- OpenBiblio (Software Libre)

- PMB (Software Libe)

- SIABUC (privativo)
 
- Aguapey (privativo)

 

 **¿Cuál es la ventaja sobre los anteriores?**

Mediante una comparativa general que realice de los mencionados anteriormente (faltaron nombrar algunos que deje a un lado, ya que quedaron obsoletos y sin soporte hace algún tiempo) pude concluir que:

- KOHA es Software Libre, primero que nada y creo que ya desde este punto, descartamos a mas de la mitad de los “competidores”.
 
- Es completamente GRATUITO, lo que haría ahorrar muchísimo dinero a la hora de su implementación.
 
- Tiene soporte actualmente y una gran comunidad detrás, se utiliza en muchos países, incluyendo Argentina. Lo que lo hace confiable en términos de soporte a largo plazo.
 
- Cumple con todos los procesos que requiere una biblioteca: Adquisiciones, procesos técnicos (catalogación y clasificación), OPAC (consulta por colección de libros con acceso en línea), prestamos, usuarios, reportes y estadísticas, impresión de etiquetas, entre otras funciones más.
 
- Los requisitos de hardware son muy pocos, corre en múltiples sistemas operativos y no requiere de una gran cantidad de recursos.

- Posee una gran cantidad de idiomas compatibles.


Por ende, llegue a la conclusión, que es un software que permite crear, es gratuito y es completo desde el punto de vista técnico. Lo cual lo hace a mi parecer, la mejor opción.

 


**¿Cuáles son las características principales de KOHA?**

Sin contar las mencionadas anteriormente, KOHA destaca también:
- Posee una interfaz muy simple y clara, tanto para el bibliotecario como para el usuario.

- Diferentes parámetros de búsquedas configurables.

- Listado de lectura y prestamos de los usuarios

- Sistema completo y simple de adquisiciones, incluyendo presupuestos e información de la tasación. Muy útil tanto para grandes como pequeñas bibliotecas.

- Posibilidad de ampliación y escalabilidad (bibliotecas con muchas sedes)

- Sistema de seriales para diarios y revistas.

- Acceso remoto al bibliotecario desde una aplicación móvil.

- Tiene un gran repertorio de informes, reportes y estadística, que se generan mediante una base de datos relacional, muy útil a la hora del análisis. 

- KOHA posee una interfaz basada en web, por lo que puede utilizarse también en máquinas esclavas donde no esté instalado el mismo, conectadas a un servidor.

- Catalogación estandarizada legible por maquinas, a nivel de campos y subcampos, utiliza formato MARC21 y UNIMARC.

 

**¿Cuál es el inconveniente que viene a solucionar?**

El problema se planteo principalmente, conversando sobre el estado que tienen los museos provinciales en la actualidad, donde en algunos casos, se estaba utilizando software muy anticuado, sin soporte y privativo, actualmente existe una ley provincial n° 13139 que exige el uso de software libre dentro de sus limites de administración, con lo cual ya estaríamos en un conflicto.

Por otro lado, en algunos de estos, siquiera existía un programa, sino que tenían los registros en papel o en planillas de base de datos, lo cual hacia extremadamente tediosa la tarea administrativa.
Cada museo, cuenta con diferentes tipos de materiales bibliográficos, en algunos casos con cantidades importantes, donde la falta de registros, llevo a que los mismos se extravíen, se dañen o siquiera se conoce la ubicación y existencia del mismo dentro de la biblioteca.

Mi intención principal es poner en una guía práctica, la implementación simple, instalación y funcionamiento, como para que cualquier persona con conocimientos intermedios o la necesidad y las ganas de implementarlo, pueda hacerlo. Y más allá de la necesidad puntual de los museos, a la cual apuntare en una segunda instancia del proyecto (su implementación), puede ser muy útil para las bibliotecas de establecimientos escolares, donde muchas de las mismas, se encuentran en esta misma situación.



**Como puntos débiles que YO destaco de KOHA, son los siguientes:**

- Complejidad a la hora de instalarlo (aunque hay bastante material extraoficial para hacerlo en diferentes sistemas, incluso privativos).

- Complejidad y tediosidad a la hora de configurar y parametrizar por primera vez, ya que desprende una lista bastante larga de opciones, que requieren un conocimiento técnico desde el punto de vista informático y a su vez técnico desde el punto de vista de catalogacion de obras literarias.

- Al ser tan completo, requiere de un conocimiento avanzado del usuario para exprimir al máximo las capacidades del programa, que son muchas.

 


**Los puntos debiles que destacan los USUARIOS en general son:**

- Se dificulta identificar las diferentes maneras de adquirir material ej: canje, donación, compra, depósito legal todo lo reconoce como adquisición.

- No emite correos a los usuarios con servicios de alerta. 

- No se puede distinguir el préstamo en sala al préstamo que esta circulado.


-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------




**OBJETIVOS**


OBJETIVO GENERAL


El objetivo principal de este trabajo, se puede agrupar en dos partes. Una primera instancia teorica donde se generara la creacion de un manual o guia intermedia de instalacion, para tecnicos o entusiastas, del programa y su configuracion inicial basica.
Y una segunda instancia donde se elegira una biblioteca y se pondra en practica el proyecto concreto.
Con este trabajo, lo que se pretende es promover una ayuda a que las pequeñas bibliotecas, que actualmente no cuenten con un sistema de gestion, o que sus sitema este obsoleto o inoperativo, puedan hacer mas practico y amigable su accionar diario.





OBJETIVOS ESPECIFICOS (divide y triunfaras)


PARTE 1

1era etapa - RECOPILACION DE MATERIAL (01/06/2019 - 30/09/2019)
Esta primer etapa, consiste en la obtencion y recopilacion de manera organizada de todo el material posible sobre KOHA, poniendonos en contacto con gente que se encuentra dentro de la comunidad actualmente, usuarios y tecnicos, para conocer no solo sus metodologias, sino tambien sus necesidades y, de ser posible, brindarles un apoyo o solucion luego dentro del manual.

2da etapa - ORGANIZACION DEL MATERIAL Y SELECCION DEL CONTENIDO (01/10/2019 - 13/10/2019)
En esta 2da etapa, lo que se pretende es la organizacion del material mas relevante y significativo para el manual, tomando todos los datos recolectados en la instancia previa, organizandolos de manera estructurada y metodica. Comenzando a darle forma a la guia practica.

3er etapa - MANUAL (14/10/2019 - 31/10/2019)
La ultima etapa de la primer parte del trabajo, es la creacion de un manual o guia practica en formato .pdf, para la instalacion, configuracion inicial y primer uso de KOHA. Con un paso a paso, de intenciones sencillas y practicas para la implementacion dentro de una distribucion GNU7Linux (Ubuntu 18.04*). Esta guia estara conformada a su vez tambien por imagenes, capturas de pantalla, aclaraciones y tips, haciendo que la misma sea atractiva y simple para quien este al otro lado.
*Se eligio Ubuntu 18.04 ya que creemos que su interfaz es la mas amigable con los usuarios finales quienes muchas veces no estan familiarizados con el software libre, por otro lado es la version mas actual de Canonical con soporte LTS.


PARTE 2

1era etapa - PREVIA IMPLEMENTACION (01/11/2019 - 10/11/2019)
Previo a la implementacion de todo lo visto en el manual obtenido, nos pondremos en contacto con una lista de bibliotecas de la localidad de Santa Fe, en la Provincia de Santa Fe (Argentina), evaluando sus condiciones actuales y la posibilidad de implementacion dentro de las mismas. Seleccionaremos una de ellas, evaluaremos el estado actual de del hardware para saber si sera optimo en su funcionamiento o hubiera que realizar algun upgrade previo antes de la imprementacion. 

2da etapa - IMPLEMENTACION (11/11/2019 - 30/11/2019)
Se procedera a la instalacion y configuracion del equipo que se utilizara en la biblioteca, capacitando en nociones basicas de uso tecnico a los/as bibliotecarios/as que vayan a utilizar el equipo con KOHA. 


PARTE 3

1era etapa - ARMADO DEL TRABAJO FINAL (01/12/2019 - 15/12/2019)
Esta sera la primer etapa de la ultima parte, donde se armara una presentacion con todo el trabajo desarrollado para exponer como proyecto final de la Tecnicatura universitaria en Software Libre de la Universidad Nacional del Litoral, ante un jurado que evaluara el mismo para la obtencion del Titulo universitario. La misma contara con una presentacion a desarrollar en un lapso temporal de entre 25 y 30 minutos, en el cual se expondran los objetivos alcanzados y el camino por el cual se obtuvieron los mismos.

2da etapa - PRESENTACION DEL TRABAJO FINAL (A informar)
La ultima etapa de la ultima parte, consiste en la presentacion de lo antes expuesto.


--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------















